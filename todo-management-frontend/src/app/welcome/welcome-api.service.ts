import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { WelcomeData } from './welcome-data';
import { AppConstants } from '../app-constants';


@Injectable()
export class WelcomeApiService {


  constructor( private httpClient: HttpClient ) { }


  getWelcomeData( email: String ): Observable< WelcomeData > {

    return this.httpClient.get( `${AppConstants.TODOS_API_URL}/welcome/${email}` );
  }

}
