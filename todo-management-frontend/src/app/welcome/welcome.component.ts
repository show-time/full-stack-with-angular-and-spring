import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WelcomeApiService } from './welcome-api.service';
import { BaseComponent } from '../base-component';
import { SimpleAuthenticationService } from '../security/simple-authentication.service';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent extends BaseComponent {


  private _userName: string;

  private _welcomeMessage: string;


  constructor( router: Router,
               simpleAuthenticationService: SimpleAuthenticationService,
               private activedRoute: ActivatedRoute,
               private welcomeApiService: WelcomeApiService ) {
    super( router, simpleAuthenticationService );
  }


  doNgOnInit(): void {

    this._userName = this.activedRoute.snapshot.params[ 'userName' ];
  }


  get userName(): string {
    return this._userName;
  }

  set userName( value: string ) {
    this._userName = value;
  }

  get welcomeMessage(): string {
    return this._welcomeMessage;
  }

  set welcomeMessage( value: string ) {
    this._welcomeMessage = value;
  }


  public getCustomWelcomeMessage(): void {

    this
      .welcomeApiService
      .getWelcomeData( this._userName )
      .subscribe(
        response => this._welcomeMessage = response.message,
      error => this.handleKO( error )
    );
  }

}
