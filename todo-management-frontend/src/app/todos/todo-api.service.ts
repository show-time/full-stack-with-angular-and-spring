import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ToDo } from './todo';
import { HttpClient } from '@angular/common/http';
import { AppConstants } from '../app-constants';


@Injectable()
export class ToDoApiService {


  constructor( private httpClient: HttpClient ) { }


  public findById( id: number ): Observable< ToDo > {

    return this.httpClient.get< ToDo >( `${AppConstants.TODOS_API_URL}/todo/${id}` );
  }


  public findByCreator( email: String ): Observable< ToDo[] > {

    return this.httpClient.get< ToDo[] >( `${AppConstants.TODOS_API_URL}/todo/creator/${email}` );
  }


  public delete( id: number ): Observable< void > {

    return this.httpClient.delete< void >( `${AppConstants.TODOS_API_URL}/todo/${id}` );
  }


  public update( todo: ToDo ): Observable< ToDo > {

    return this.httpClient.put< ToDo >( `${AppConstants.TODOS_API_URL}/todo/${todo.id}`, todo );
  }


  public create( todo: ToDo ): Observable< void > {

    return this.httpClient.post< void >( `${AppConstants.TODOS_API_URL}/todo/`, todo );
  }

}
