import { Component, OnInit } from '@angular/core';
import { ToDoApiService } from '../todo-api.service';
import { ToDo } from '../todo';
import { BaseComponent } from '../../base-component';
import { SimpleAuthenticationService } from '../../security/simple-authentication.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-todo-update',
  templateUrl: './todo-update.component.html',
  styleUrls: ['./todo-update.component.css']
})
export class TodoUpdateComponent extends BaseComponent {


  private _id: number;

  private _todo: ToDo;


  constructor( router: Router,
               simpleAuthenticationService: SimpleAuthenticationService,
               private activatedRoute: ActivatedRoute,
               private todoApiService: ToDoApiService ) {
    super( router, simpleAuthenticationService );
  }


  doNgOnInit(): void {

    this._id = this.activatedRoute.snapshot.params[ 'id' ];

    this._todo = this.initialValueOfTodo();

    this
      .todoApiService
      .findById( this._id )
      .subscribe(
        response => this._todo = response,
        error => this.handleKO(error)
      );
  }


  private initialValueOfTodo() {

    return new ToDo( -1, '', false, null, new Date(), this.simpleAuthenticationService.getCurrentUser() );
  }


  get id(): number {
    return this._id;
  }

  set id( value: number ) {
    this._id = value;
  }

  get todo(): ToDo {
    return this._todo;
  }

  set todo( value: ToDo ) {
    this._todo = value;
  }


  public submitTodoForm() {

    this
      .todoApiService
      .update( this._todo )
      .subscribe(
        response => { this.handleOK(); this._todo = response; },
        error => this.handleKO( error )
      );
  }


  public goToList(): void {

    this.navigateTo( [ 'todo-list' ] );
  }

}
