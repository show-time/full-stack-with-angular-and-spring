import { Component, OnInit } from '@angular/core';
import { ToDo } from '../todo';
import { SimpleAuthenticationService } from '../../security/simple-authentication.service';
import { Router } from '@angular/router';
import { ToDoApiService } from '../todo-api.service';
import { BaseComponent } from '../../base-component';


@Component({
  selector: 'app-todo-create',
  templateUrl: './todo-create.component.html',
  styleUrls: ['./todo-create.component.css']
})
export class TodoCreateComponent extends BaseComponent {


  private _todo: ToDo;


  constructor( router: Router,
               simpleAuthenticationService: SimpleAuthenticationService,
               private todoApiService: ToDoApiService ) {
    super( router, simpleAuthenticationService );
  }


  doNgOnInit() {

    this._todo = this.initialValueOfTodo();
  }


  private initialValueOfTodo() {

    return new ToDo( -1, '', false, null, new Date(), this.simpleAuthenticationService.getCurrentUser() );
  }


  get todo(): ToDo {
    return this._todo;
  }

  set todo( value: ToDo ) {
    this._todo = value;
  }


  public submitTodoForm() {

    this
      .todoApiService
      .create( this._todo )
      .subscribe(
        response => this.handleOK(),
        error => this.handleKO( error ),
      );
  }


  public goToList(): void {

    this.navigateTo( [ 'todo-list' ] );
  }

}
