import { Component, OnInit } from '@angular/core';
import { ToDo } from '../todo';
import { ToDoApiService } from '../todo-api.service';
import { BaseComponent } from '../../base-component';
import { SimpleAuthenticationService } from '../../security/simple-authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent extends BaseComponent {


  private _todoList: Array< ToDo >;


  constructor( router: Router,
               simpleAuthenticationService: SimpleAuthenticationService,
               private todoApiService: ToDoApiService ) {
    super( router, simpleAuthenticationService );
  }


  doNgOnInit(): void {

    this.retrieveTodoList();
  }


  get todoList(): Array<ToDo> {
    return this._todoList;
  }

  set todoList( value: Array<ToDo> ) {
    this._todoList = value;
  }


  private retrieveTodoList(): void {

    this
      .todoApiService
      .findByCreator( this.simpleAuthenticationService.getLoggedInUserEmail() )
      .subscribe(
        response => this._todoList = response,
        error => this.handleKO( error )
      );
  }


  public deleteToDo( id: number ): void {

    this
      .todoApiService
      .delete( id )
      .subscribe(
        response => this.retrieveTodoList(),
        error => this.handleKO( error )
      );
  }


  public updateToDo( id: number ): void {

    this.navigateTo( [ 'todo-update', id ] );
  }


  public createToDo(): void {

    this.navigateTo( [ 'todo-create' ] );
  }

}
