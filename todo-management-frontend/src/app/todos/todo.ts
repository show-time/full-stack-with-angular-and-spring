import { User } from '../security/user';

export class ToDo {

  constructor(
    public id: number,
    public description: string,
    public done: boolean,
    public targetDate: Date,
    public creationDate: Date,
    public creator: User ) {
  }

}
