import { Component } from '@angular/core';
import { SimpleAuthenticationService } from './security/simple-authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
