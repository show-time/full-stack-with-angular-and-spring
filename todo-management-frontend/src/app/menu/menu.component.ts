import { Component } from '@angular/core';
import { BaseComponent } from '../base-component';
import { SimpleAuthenticationService } from '../security/simple-authentication.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent extends BaseComponent {


  constructor( router: Router, simpleAuthenticationService: SimpleAuthenticationService ) {
    super( router, simpleAuthenticationService );
  }


  doNgOnInit(): void {
  }

}
