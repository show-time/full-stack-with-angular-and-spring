import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SimpleAuthenticationService } from './simple-authentication.service';


@Injectable()
export class RouteGuardService implements CanActivate {


  constructor( private simpleAuthenticationService: SimpleAuthenticationService,
               private router: Router ) { }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if( this.simpleAuthenticationService.isUserLoggedIn() ) {

      return true;
    }

    this.router.navigate( ['submitLoginForm'] );

    return false;
  }

}
