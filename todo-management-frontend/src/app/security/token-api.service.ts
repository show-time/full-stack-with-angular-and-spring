import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppConstants } from '../app-constants';
import { Response } from './static_response';


@Injectable()
export class TokenApiService {


  // note that we use here the Http service not the HttpClient service ir order to avoid circular dependencies
  // between this service and the appliaction interceptor which is configured for the HttpClient
  constructor( private http: Http ) { }


  public generateToken( email: string, password: string ): Observable< Response > {

    const headers = this.buildGenerateHeaders( email, password );

    const options = new RequestOptions( { headers: headers } );

    return this.http.get( `${AppConstants.TOKEN_API_URL}/token/generate`, options );
  }


  public refreshToken( token: string ): Observable< Response > {

    const headers = this.buildRefreshHeaders( token );

    const options = new RequestOptions( { headers: headers } );

    return this.http.get( `${AppConstants.TOKEN_API_URL}/token/refresh`, options );
  }


  private buildGenerateHeaders( email: string, password: string ): Headers {

    const headers = new Headers();

    headers.append( 'email', `${email}` );

    headers.append( 'password', `${password}` );

    return headers;
  }


  private buildRefreshHeaders( token: string ): Headers {

    const headers = new Headers();

    headers.append( 'Authorization', `${token}` );

    return headers;
  }

}
