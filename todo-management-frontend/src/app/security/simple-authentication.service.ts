import { Injectable } from '@angular/core';
import { UserApiService } from './user-api.service';
import { User } from './user';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class SimpleAuthenticationService {


  constructor( private userApiService: UserApiService ) { }


  public logIn( email: string, password: string ): Observable< number > {

    this.logOut();

    return this
      .userApiService
      .findUserIdByEmailAndPassword( email, password )
      .pipe(
        map(
          userId => {
            sessionStorage.setItem( 'todo-management-app-logged-in-user-id', userId.toString() );
            sessionStorage.setItem( 'todo-management-app-logged-in-user-email', email );
            return userId;
          }
        )
      );
  }


  public logOut() {
    sessionStorage.removeItem( 'todo-management-app-logged-in-user-id' );
    sessionStorage.removeItem( 'todo-management-app-logged-in-user-email' );
  }


  public isUserLoggedIn(): boolean {

    const loggedInEmail = this.getLoggedInUserEmail();

    return !( loggedInEmail === null );
  }


  public getLoggedInUserId(): string {

    return sessionStorage.getItem( 'todo-management-app-logged-in-user-id' );
  }


  public getLoggedInUserEmail(): string {

    return sessionStorage.getItem( 'todo-management-app-logged-in-user-email' );
  }


  public getCurrentUser(): User {

    return new User( Number.parseInt( this.getLoggedInUserId() ), this.getLoggedInUserEmail() );
  }

}
