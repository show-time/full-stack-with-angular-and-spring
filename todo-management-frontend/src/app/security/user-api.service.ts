import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { User } from './user';
import { AppConstants } from '../app-constants';


@Injectable()
export class UserApiService {


  constructor( private httpClient: HttpClient ) { }


  public findByEmail( email: string ): Observable< User > {

    return this.httpClient.get( `${AppConstants.TODOS_API_URL}/user/email/${email}` );
  }


  public findUserIdByEmailAndPassword( email: string, password: string ): Observable< number > {

    const b64pwd = window.btoa( password );

    return this.httpClient.get( `${AppConstants.TODOS_API_URL}/user/email/${email}/password/${b64pwd}` );
  }

}
