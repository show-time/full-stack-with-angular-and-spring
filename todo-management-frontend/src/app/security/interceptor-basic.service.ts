
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

import {
  HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,
  HttpResponse, HttpResponseBase
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TokenApiService } from './token-api.service';


@Injectable()
export class InterceptorBasicService implements HttpInterceptor {


  constructor( private tokenApiService: TokenApiService ) { }


  public intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent< any > > {

    // let's add to the request the authorization header ( for api security )

    const authorizationHeader = this.createBasicAuthenticationHeader();

    const req: HttpRequest<any> = request.clone({ setHeaders: { Authorization: authorizationHeader } });

    // let's now launch the prepared request and apply the treatment of 201, 204, etc... status codes
    return next
            .handle( req )
            .catch( response => {
              if (response instanceof HttpErrorResponse) {

                // Check if this error has a 2xx status
                if (this.is2xxStatus(response)) {

                  // Convert the error to a success response with a null body and then return
                  return Observable.of(new HttpResponse({
                        headers: response.headers,
                        status: response.status,
                        statusText: response.statusText,
                        url: response.url
                  }));
                }
              }

              // that is a real error, rethrow
              return Observable.throw( response );
            });
  }


  private createBasicAuthenticationHeader() {

    const user = 'user';

    const password = 'password';

    return 'Basic ' + window.btoa( user + ':' + password );
  }


  private is2xxStatus( response: HttpResponseBase ) {

    return response.status >= 200 && response.status < 300;
  }

}
