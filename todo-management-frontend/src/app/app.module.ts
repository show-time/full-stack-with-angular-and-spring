import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { ErrorComponent } from './error/error.component';
import { LoginComponent } from './login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { TodoListComponent } from './todos/todo-list/todo-list.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { SimpleAuthenticationService } from './security/simple-authentication.service';
import { LogoutComponent } from './logout/logout.component';
import { RouteGuardService } from './security/route-guard.service';
import { WelcomeApiService } from './welcome/welcome-api.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ToDoApiService } from './todos/todo-api.service';
import { TodoUpdateComponent } from './todos/todo-update/todo-update.component';
import { TodoCreateComponent } from './todos/todo-create/todo-create.component';
import { UserApiService } from './security/user-api.service';
import { InterceptorBasicService } from './security/interceptor-basic.service';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'submitLoginForm', component: LoginComponent },
  { path: 'error', component: ErrorComponent },
  { path: 'welcome/:userName', component: WelcomeComponent, canActivate: [ RouteGuardService ] },
  { path: 'todo-list', component: TodoListComponent, canActivate: [ RouteGuardService ]  },
  { path: 'todo-create', component: TodoCreateComponent, canActivate: [ RouteGuardService ]  },
  { path: 'todo-update/:id', component: TodoUpdateComponent, canActivate: [ RouteGuardService ]  },
  { path: 'logout', component: LogoutComponent, canActivate: [ RouteGuardService ]  },
  { path: '**', component: ErrorComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    WelcomeComponent,
    ErrorComponent,
    TodoListComponent,
    MenuComponent,
    FooterComponent,
    LogoutComponent,
    TodoUpdateComponent,
    TodoCreateComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot( routes ),
    FormsModule,
    HttpClientModule
  ],
  providers: [
    SimpleAuthenticationService,
    RouteGuardService,
    WelcomeApiService,
    ToDoApiService,
    UserApiService,
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorBasicService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
