import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SimpleAuthenticationService } from '../security/simple-authentication.service';
import { BaseComponent } from '../base-component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends BaseComponent {


  private _email: string;

  private _password: string;

  private _illegalCredentials: boolean;


  constructor( router: Router, simpleAuthenticationService: SimpleAuthenticationService ) {
    super( router, simpleAuthenticationService );
  }


  doNgOnInit(): void {
  }


  get email(): string {
    return this._email;
  }

  set email( value: string ) {
    this._email = value;
  }

  get password(): string {
    return this._password;
  }

  set password( value: string ) {
    this._password = value;
  }

  get illegalCredentials(): boolean {
    return this._illegalCredentials;
  }

  set illegalCredentials( value: boolean ) {
    this._illegalCredentials = value;
  }


  public submitLoginForm(): void {

    this
      .simpleAuthenticationService
      .logIn( this._email, this._password )
      .subscribe(
        response => this.navigateTo( [ 'welcome', this._email ] ),
        error => this._illegalCredentials = true
      );
  }

}
