import { Component } from '@angular/core';
import { SimpleAuthenticationService } from '../security/simple-authentication.service';
import { Router } from '@angular/router';
import { BaseComponent } from '../base-component';


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent extends BaseComponent {


  constructor( router: Router, simpleAuthenticationService: SimpleAuthenticationService ) {
    super( router, simpleAuthenticationService );
  }


  doNgOnInit(): void {

    this.simpleAuthenticationService.logOut();
  }

}
