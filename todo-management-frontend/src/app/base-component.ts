import { SimpleAuthenticationService } from './security/simple-authentication.service';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';


/**
 * Base class for components.
 *
 */
export abstract class BaseComponent implements OnInit {


  private _requestSent: boolean;

  private _backendError: Error;

  protected router: Router;

  protected simpleAuthenticationService: SimpleAuthenticationService;


  constructor( router: Router,
               simpleAuthenticationService: SimpleAuthenticationService ) {
    this.router = router;
    this.simpleAuthenticationService = simpleAuthenticationService;
  }


  ngOnInit() {

    this._requestSent = false;

    this.doNgOnInit();
  }


  abstract doNgOnInit(): void;


  get requestSent(): boolean {
    return this._requestSent;
  }

  set requestSent( value: boolean ) {
    this._requestSent = value;
  }

  get backendError(): Error {
    return this._backendError;
  }

  set backendError( value: Error ) {
    this._backendError = value;
  }


  protected handleOK() {

    this._requestSent = true;

    this._backendError = null;
  }


  protected handleKO( error: Error ) {

    this._requestSent = true;

    this._backendError = error;
  }


  public isUserLoggedIn(): boolean {

    return this.simpleAuthenticationService.isUserLoggedIn();
  }


  public navigateTo( commands: any[] ): void {

    this.router.navigate( commands );
  }

}
