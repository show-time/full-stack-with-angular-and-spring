package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.tools;

import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.Constants;
import org.hibernate.engine.jdbc.internal.DDLFormatterImpl;
import org.hibernate.jpa.AvailableSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import javax.persistence.Persistence;
import javax.sql.DataSource;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


/**
 * This tool generates the DDL of the model.
 *
 * @author BBVA.
 */
@SpringBootApplication
@EntityScan( basePackages = {Constants.ENTITIES_BASE_PACKAGE} )
public class GenerateDDLTool {


    public static final String PROFILE = "GenerateDDLTool";


    public static void main( String... args ) {

        SpringApplication application = new SpringApplication( GenerateDDLTool.class );

        application.setWebApplicationType( WebApplicationType.NONE );

        application.setAdditionalProfiles( PROFILE );

        application.run( args );
    }


    @Bean
    @Profile( PROFILE )
    public GenerateDDLToolCLR generateDDLToolCLR() {

        return new GenerateDDLToolCLR();
    }


    static class GenerateDDLToolCLR implements CommandLineRunner {

        @Autowired DataSource dataSource;


        @Override
        public void run( String... strings ) throws Exception {

            Properties properties = new Properties();

            properties.setProperty( org.hibernate.cfg.AvailableSettings.HBM2DDL_AUTO, "" );

            properties.setProperty( org.hibernate.cfg.AvailableSettings.DIALECT, HibernateDialect.POSTGRES.getDialectClass() );

//            properties.setProperty( AvailableSettings.NAMING_STRATEGY, SpringPhysicalNamingStrategy.class.getName() );

            properties.setProperty( AvailableSettings.SCHEMA_GEN_DATABASE_ACTION, "none" );

            properties.setProperty( AvailableSettings.SCHEMA_GEN_SCRIPTS_ACTION, "drop-and-create" );

            properties.setProperty( AvailableSettings.SCHEMA_GEN_CREATE_SOURCE, "metadata" );

            properties.put( AvailableSettings.SCHEMA_GEN_CONNECTION, this.dataSource.getConnection() );

            StringWriter createWriter = new StringWriter();

            properties.put( AvailableSettings.SCHEMA_GEN_SCRIPTS_CREATE_TARGET, createWriter );

            StringWriter dropWriter = new StringWriter();

            properties.put( AvailableSettings.SCHEMA_GEN_SCRIPTS_DROP_TARGET, dropWriter );

            Persistence.generateSchema( Constants.PERSISTENCE_UNIT_NAME, properties );

            this.prettyPrint( createWriter.toString(), this.createDdlFileName() );

            this.prettyPrint( dropWriter.toString(), this.dropDdlFileName() );
        }


        private void prettyPrint( final String unformattedSql, final String fileName ) throws IOException {

            //let's actually delete the file as the intent is to regenerate it
            File ddlFile = new File( fileName );

            if( ddlFile.exists() ) {

                ddlFile.delete();
            }

            ddlFile.createNewFile();

            //let's transform each line of the unformatted sql into a formatted line
            BufferedReader bufferedReader = new BufferedReader( new StringReader( unformattedSql ) );

            DDLFormatterImpl formatter = new DDLFormatterImpl();

            List< String >  lines = new ArrayList<>();

            String line;

            while( ( line = bufferedReader.readLine() ) != null ) {

                String formatted = formatter.format( line + ";" );

                lines.add( formatted );

//                lines.add( System.lineSeparator() );
            }

            //let's actually write all the lines at once
            Files.write( ddlFile.toPath(), lines, StandardOpenOption.APPEND );
        }


        private String createDdlFileName() {

            Path currentRelativePath = Paths.get("");

            return currentRelativePath.toAbsolutePath().toString() + "/tools/sql/create.sql";
        }


        private String dropDdlFileName() {

            Path currentRelativePath = Paths.get("");

            return currentRelativePath.toAbsolutePath().toString() + "/tools/sql/drop.sql";
        }
    }


}
