package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.tools;

import com.github.javafaker.Faker;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities.ToDo;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities.User;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities.UserSetting;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.repos.ToDoRepository;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;
import java.util.Locale;
import java.util.UUID;


@SpringBootApplication
public class PopulateDatabaseTool {


    public static final String PROFILE = "PopulateDatabaseTool";


    public static void main( String... args ) {

        SpringApplication application = new SpringApplication( PopulateDatabaseTool.class );

        application.setWebApplicationType( WebApplicationType.NONE );

        application.setAdditionalProfiles( PROFILE );

        application.run( args );
    }


    @Bean
    public PasswordEncoder passwordEncoder() {

        return new BCryptPasswordEncoder();
    }


    @Bean
    @Profile( PROFILE )
    @Autowired
    public PopulateDatabaseToolCLR populateDatabaseToolCLR( UserRepository userRepository,
                                                            ToDoRepository toDoRepository,
                                                            PasswordEncoder passwordEncoder ) {

        return new PopulateDatabaseToolCLR( userRepository, toDoRepository, passwordEncoder );
    }


    static class PopulateDatabaseToolCLR implements CommandLineRunner {


        private final UserRepository userRepository;

        private final ToDoRepository toDoRepository;

        private final PasswordEncoder passwordEncoder;

        private final Faker faker;


        public PopulateDatabaseToolCLR( UserRepository userRepository,
                                        ToDoRepository toDoRepository,
                                        PasswordEncoder passwordEncoder ) {
            this.userRepository = userRepository;
            this.toDoRepository = toDoRepository;
            this.passwordEncoder = passwordEncoder;
            this.faker = new Faker( new Locale( "es" ) );
        }


        @Override
        public void run( String... strings ) throws Exception {

            deleteAllData();


            User user1 = new User();

            user1.setEmail( "user1@example.com" );

            user1.setPassword( new String( this.passwordEncoder.encode( "user1" ) ) );

            user1.setActive( true );

            user1.addAlphanumericSetting( UserSetting.UserSettingName.beer.toString(), faker.beer().name() );

            user1.addAlphanumericSetting( UserSetting.UserSettingName.color.toString(), faker.color().name() );

            user1.addAlphanumericSetting( UserSetting.UserSettingName.code.toString(), faker.code().isbn13() );

            user1.addAlphanumericSetting( UserSetting.UserSettingName.superhero.toString(), faker.superhero().name() );

            user1.addAlphanumericSetting( UserSetting.UserSettingName.chuck_fact.toString(), faker.chuckNorris().fact() );

            user1.addAlphanumericSetting( UserSetting.UserSettingName.address.toString(), faker.address().streetAddress( true ) );

            user1 = this.userRepository.save( user1 );


            ToDo toDo = new ToDo();

            toDo.setCreationDate( new Date() );

            toDo.setTargetDate( new Date() );

            toDo.setCreator( user1 );

            toDo.setDone(false);

            toDo.setDescription( "Conquest the world");

            this.toDoRepository.save(toDo);

            toDo = new ToDo();

            toDo.setCreationDate( new Date() );

            toDo.setTargetDate( new Date() );

            toDo.setCreator( user1 );

            toDo.setDone(false);

            toDo.setDescription( "Dominate humanity");

            this.toDoRepository.save(toDo);

            toDo = new ToDo();

            toDo.setCreationDate( new Date() );

            toDo.setTargetDate( new Date() );

            toDo.setCreator( user1 );

            toDo.setDone(false);

            toDo.setDescription( "Learn more");

            this.toDoRepository.save(toDo);


            toDo = new ToDo();

            toDo.setCreationDate( new Date() );

            toDo.setTargetDate( new Date() );

            toDo.setCreator( user1 );

            toDo.setDone(false);

            toDo.setDescription( "Have a rest");

            this.toDoRepository.save(toDo);


            User user2 = new User();

            user2.setEmail( "user2@example.com" );

            user2.setPassword( new String( this.passwordEncoder.encode( "user2" ) ) );

            user2.setActive( true );

            user2.addAlphanumericSetting( UserSetting.UserSettingName.beer.toString(), faker.beer().name() );

            user2.addAlphanumericSetting( UserSetting.UserSettingName.color.toString(), faker.color().name() );

            user2.addAlphanumericSetting( UserSetting.UserSettingName.code.toString(), faker.code().isbn13() );

            user2.addAlphanumericSetting( UserSetting.UserSettingName.superhero.toString(), faker.superhero().name() );

            user2.addAlphanumericSetting( UserSetting.UserSettingName.chuck_fact.toString(), faker.chuckNorris().fact() );

            user2.addAlphanumericSetting( UserSetting.UserSettingName.address.toString(), faker.address().streetAddress( true ) );

            user2 = this.userRepository.save( user2 );


            toDo = new ToDo();

            toDo.setCreationDate( new Date() );

            toDo.setTargetDate( new Date() );

            toDo.setCreator( user2 );

            toDo.setDone(false);

            toDo.setDescription( "Learn philosophy");

            this.toDoRepository.save(toDo);

            toDo = new ToDo();

            toDo.setCreationDate( new Date() );

            toDo.setTargetDate( new Date() );

            toDo.setCreator( user2 );

            toDo.setDone(false);

            toDo.setDescription( "Do differential geometry exercises");

            this.toDoRepository.save(toDo);

            toDo = new ToDo();

            toDo.setCreationDate( new Date() );

            toDo.setTargetDate( new Date() );

            toDo.setCreator( user2 );

            toDo.setDone(false);

            toDo.setDescription( "Have some fun");

            this.toDoRepository.save(toDo);

            toDo = new ToDo();

            toDo.setCreationDate( new Date() );

            toDo.setTargetDate( new Date() );

            toDo.setCreator( user2 );

            toDo.setDone(false);

            toDo.setDescription( "Learn particle physics");

            this.toDoRepository.save(toDo);

            toDo = new ToDo();

            toDo.setCreationDate( new Date() );

            toDo.setTargetDate( new Date() );

            toDo.setCreator( user2 );

            toDo.setDone(false);

            toDo.setDescription( "Create a black hole");

            this.toDoRepository.save(toDo);

            toDo = new ToDo();

            toDo.setCreationDate( new Date() );

            toDo.setTargetDate( new Date() );

            toDo.setCreator( user2 );

            toDo.setDone(false);

            toDo.setDescription( "Create a white hole");

            this.toDoRepository.save(toDo);

            toDo = new ToDo();

            toDo.setCreationDate( new Date() );

            toDo.setTargetDate( new Date() );

            toDo.setCreator( user2 );

            toDo.setDone(false);

            toDo.setDescription( "Join both holes to create a swarm hole");

            this.toDoRepository.save(toDo);

            toDo = new ToDo();

            toDo.setCreationDate( new Date() );

            toDo.setTargetDate( new Date() );

            toDo.setCreator( user2 );

            toDo.setDone(false);

            toDo.setDescription( "Travel to another universe");

            this.toDoRepository.save(toDo);
        }


        private void deleteAllData() {

            this.toDoRepository.deleteAll();

            this.userRepository.deleteAll();
        }
    }

}
