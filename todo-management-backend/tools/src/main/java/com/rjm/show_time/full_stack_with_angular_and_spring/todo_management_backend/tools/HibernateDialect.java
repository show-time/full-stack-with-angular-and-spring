package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.tools;


public enum HibernateDialect {

    POSTGRES( org.hibernate.dialect.PostgreSQL82Dialect.class.getName() ),

    MYSQL( "org.hibernate.dialect.MySQLDialect" ),

    HSQL( "org.hibernate.dialect.HSQLDialect" ),

    H2( "org.hibernate.dialect.H2Dialect" );


    private String dialectClass;


    private HibernateDialect( final String dialectClass ) {

        this.dialectClass = dialectClass;
    }


    public String getDialectClass() {

        return this.dialectClass;
    }

}
