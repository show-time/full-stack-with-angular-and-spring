
    alter table ToDo 
       drop constraint FKcrd7v51kgmoijjlxiv6sia3vc;

    alter table UserSetting 
       drop constraint FK9kx1sy2gwow8u9kcq2gyic9wn;

    drop table if exists ToDo cascade;

    drop table if exists UserSetting cascade;

    drop table if exists uxer cascade;
