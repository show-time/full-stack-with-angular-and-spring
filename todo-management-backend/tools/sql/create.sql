
    create table ToDo (
        id uuid not null,
        creationDate timestamp,
        description varchar(255),
        done boolean,
        targetDate timestamp,
        creator_id uuid not null,
        primary key (id)
    );

    create table UserSetting (
        id uuid not null,
        clazz varchar(255) not null,
        name varchar(255) not null,
        value varchar(1024) not null,
        user_id uuid not null,
        primary key (id)
    );

    create table uxer (
        id uuid not null,
        active boolean not null,
        email varchar(255) not null,
        password varchar(255) not null,
        primary key (id)
    );

    alter table UserSetting 
       add constraint name_user_id_constraint unique (name, user_id);

    alter table uxer 
       add constraint UK_7rnl6hi55d65nk6h7csb252ky unique (email);

    alter table ToDo 
       add constraint FKcrd7v51kgmoijjlxiv6sia3vc 
       foreign key (creator_id) 
       references uxer;

    alter table UserSetting 
       add constraint FK9kx1sy2gwow8u9kcq2gyic9wn 
       foreign key (user_id) 
       references uxer;
