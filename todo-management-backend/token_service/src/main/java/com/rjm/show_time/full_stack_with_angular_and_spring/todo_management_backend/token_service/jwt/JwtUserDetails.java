package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.token_service.jwt;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class JwtUserDetails implements UserDetails {


	private static final long serialVersionUID = 5155720064139820502L;


	private final String username;

	private final String password;

	private final Collection< SimpleGrantedAuthority > authorities;


	public JwtUserDetails( String username, String password ) {
		super();

		this.username = username;

		this.password = password;

		this.authorities = new ArrayList<>();

		this.authorities.add( new SimpleGrantedAuthority( "nothing" ) );
	}


	@Override
	public String getUsername() {
		return username;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {

		return authorities;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
