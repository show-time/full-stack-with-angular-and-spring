package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.token_service.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class TokenRequest {

    private String email;

    private String password;
}
