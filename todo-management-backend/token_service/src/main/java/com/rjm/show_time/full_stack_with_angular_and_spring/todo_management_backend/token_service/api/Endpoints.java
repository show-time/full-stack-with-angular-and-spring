package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.token_service.api;


public class Endpoints {


    public static final String API_BASE = "/api";


    public static final String TOKEN_BASE = API_BASE + "/" + "token";

    public static final String GENERATE_TOKEN = TOKEN_BASE + "/" + "generate";

    public static final String REFRESH_TOKEN = TOKEN_BASE + "/" + "refresh";
}
