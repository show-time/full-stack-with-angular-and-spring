package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.token_service.api;


import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities.User;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.repos.UserRepository;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.token_service.jwt.JwtTokenUtil;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.token_service.jwt.JwtUserDetails;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
public class TokenRestController {


    private final UserRepository userRepository;

    private final JwtTokenUtil jwtTokenUtil;

    private final PasswordEncoder passwordEncoder;


    public TokenRestController( UserRepository userRepository,
                                JwtTokenUtil jwtTokenUtil,
                                PasswordEncoder passwordEncoder ) {
        super();

        this.userRepository = userRepository;

        this.jwtTokenUtil = jwtTokenUtil;

        this.passwordEncoder = passwordEncoder;
    }


    @GetMapping( value = Endpoints.GENERATE_TOKEN )
    public ResponseEntity< String > generateToken( @RequestHeader( name = "email" ) String email,
                                                   @RequestHeader( name = "password" ) String password ) {

        Optional< User > optionalUser = this.userRepository.findByEmail( email );

        if( !optionalUser.isPresent() ) {

            return ResponseEntity.status( HttpStatus.UNAUTHORIZED ).body( "User email not found" );
        }

        User user = optionalUser.get();

        if( !this.passwordEncoder.matches( password, user.getPassword() ) ) {

            return ResponseEntity.status( HttpStatus.UNAUTHORIZED ).body( "Invalid password" );
        }

        JwtUserDetails userDetails = new JwtUserDetails( user.getEmail(), user.getPassword() );

        return ResponseEntity.ok( this.jwtTokenUtil.generateToken( userDetails ) );
    }


    @GetMapping( value = Endpoints.REFRESH_TOKEN )
    public ResponseEntity< String > refreshToken( @RequestHeader( name = "Authorization" ) String authHeader ) {

        final String token = authHeader.substring( "Bearer".length() );

        String username = this.jwtTokenUtil.getUsernameFromToken( token );

        Optional< User > optionalUser = this.userRepository.findByEmail( username );

        if( !optionalUser.isPresent() ) {

            return ResponseEntity.status( HttpStatus.UNAUTHORIZED ).body( "No valid user found in token" );
        }

        if( this.jwtTokenUtil.canTokenBeRefreshed( token ) ) {

            return ResponseEntity.ok( this.jwtTokenUtil.refreshToken( token ) );
        } else {

            return ResponseEntity.badRequest().build();
        }
    }

}
