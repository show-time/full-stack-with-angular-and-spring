package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain;


public class Constants {


    public static final String PERSISTENCE_UNIT_NAME = "todo-management-back";

    public static final String REPOS_BASE_PACKAGE = "com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.repos";

    public static final String ENTITIES_BASE_PACKAGE = "com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities";

}
