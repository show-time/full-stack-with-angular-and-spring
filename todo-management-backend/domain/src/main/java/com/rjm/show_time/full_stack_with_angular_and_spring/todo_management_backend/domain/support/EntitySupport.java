package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.support;

import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities.BaseEntity;

import javax.persistence.Entity;
import java.util.stream.Stream;


public class EntitySupport {

    /**
     * Returns an optional entity name out of the specified <code>clazz</code>. An empty string is returned if the
     * specified <code>clazz</code> is not annotated with the {@link Entity} annotation.
     *
     * @param clazz  some class.
     * @return  an optional entity name.
     */
    public static < T extends BaseEntity > String entityName( Class< T > clazz ) {

        return Stream
                .of( clazz.getAnnotations() )
                .filter( annotation -> annotation instanceof Entity )
                .map( annotation -> ( Entity )annotation )
                .map( entity -> entity.name() )
                .findFirst()
                .orElse("" );
    }
}
