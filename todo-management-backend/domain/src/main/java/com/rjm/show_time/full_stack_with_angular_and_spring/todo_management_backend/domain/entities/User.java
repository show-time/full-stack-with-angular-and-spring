package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.valid4j.Assertive.require;


@Entity
@Table( name = "uxer" )
@Data
public class User extends BaseEntity {


    @Column( nullable = false, unique = true )
    private String email;

    @Column( nullable = false )
    private String password;

    @Column( nullable = false )
    private Boolean active;

    @OneToMany(
            orphanRemoval = true,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "user"
    )
    private List< UserSetting > settings = new ArrayList<>();


    public User() {
        super();
    }


    public User( UUID id ) {
        super( id );
    }


    private void addSetting( String name, String value ) {

        require( name, notNullValue() );

        require( value, notNullValue() );

        UserSetting setting = new UserSetting();

        setting.setUser( this );

        setting.setName( name );

        setting.setValue( value );

        setting.setClazz( value.getClass().getName() );

        this.settings.add( setting );
    }


    public void addAlphanumericSetting( String name, String value ) {

        addSetting( name, value );
    }


    public < T extends Number > void addNumericSetting( String name, T value ) {

        addSetting( name, value.toString() );
    }

 }
