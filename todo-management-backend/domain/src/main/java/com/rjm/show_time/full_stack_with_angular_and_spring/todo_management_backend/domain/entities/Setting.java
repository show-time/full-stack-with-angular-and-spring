package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.UUID;


@Data
@MappedSuperclass
public abstract class Setting extends BaseEntity {


    @Column( nullable = false )
    private String name;

    @Column( nullable = false, length = 1024 )
    private String value;

    @Column( nullable = false )
    private String clazz;


    protected Setting() {
        super();
    }


    protected Setting( UUID id ) {
        super( id );
    }

}
