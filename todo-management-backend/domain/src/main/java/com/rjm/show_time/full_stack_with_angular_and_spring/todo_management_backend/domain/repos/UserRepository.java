package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.repos;

import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;


@Transactional( readOnly = true )
public interface UserRepository extends JpaRepository< User, UUID > {


    @Query( "from User u where u.email = :email" )
    Optional< User > findByEmail( @Param( value = "email" ) String email );

    @Query( "from User u where u.email = :email and u.password = :password" )
    Optional< User > findByEmailAndPassword( @Param( value = "email" ) String email,
                                             @Param( value = "password" ) String password );

    @Query( "select u.id from User u where u.email = :email and u.password = :password" )
    Optional< Long > findIdByEmailAndPassword( @Param( value = "email" ) String email,
                                               @Param( value = "password" ) String password );
}
