package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.UUID;


@Entity
@Data
public class ToDo extends BaseEntity {


    private String description;

    private Boolean done;

    private Date targetDate;

    private Date creationDate;

    @ManyToOne( optional = false )
    private User creator;


    public ToDo() {
        super();
    }


    public ToDo( UUID id ) {
        super( id );
    }
}
