package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

import static org.hamcrest.core.IsNull.notNullValue;
import static org.valid4j.Assertive.require;


/**
 * Base class for entities.
 *
 */
@EqualsAndHashCode( of = { "id" } )
@MappedSuperclass
public abstract class BaseEntity implements Serializable, Persistable< UUID > {


    @Getter
    @Id
    @Column( name = "id", length = 16, unique = true, nullable = false )
	private UUID id;


    @Getter
    @Transient
    private Boolean persisted;


    @PostPersist
    @PostLoad
    public void onPostPersistOnPostLoad() {

        this.persisted = true;
    }


    @Override
    public boolean isNew() {

        return !this.persisted;
    }


    /**
     * Creates a new instance of this class which represents an entity not persisted to the database.
     *
     */
    protected BaseEntity() {
        super();

        this.id = UUID.randomUUID();

        this.persisted = false;
    }


    /**
     * Creates a new instance of this class which represents an entity already persisted in the database with the
     * specified <code>id</code>.
     *
     * @param id  an id.
     */
    protected BaseEntity( UUID id ) {
        super();

        require( id, notNullValue() );

        this.id = id;

        this.persisted = true;
    }

}
