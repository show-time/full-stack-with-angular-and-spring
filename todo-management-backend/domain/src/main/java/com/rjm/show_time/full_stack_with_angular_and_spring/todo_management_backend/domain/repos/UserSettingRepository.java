package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.repos;

import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities.UserSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;


@Transactional( readOnly = true )
public interface UserSettingRepository extends JpaRepository< UserSetting, UUID> {
}
