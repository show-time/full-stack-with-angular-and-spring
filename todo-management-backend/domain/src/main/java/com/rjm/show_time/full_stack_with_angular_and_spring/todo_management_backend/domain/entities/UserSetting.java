package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.UUID;


@Entity
@Data
@Table(
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "name_user_id_constraint",
                        columnNames = { "name", "user_id" }
                )
        }
)
public class UserSetting extends Setting {


    public enum UserSettingName {
        beer,
        color,
        code,
        superhero,
        chuck_fact,
        address;
    }

    @ManyToOne(
            optional = false,
            fetch = FetchType.LAZY
    )
    private User user;


    public UserSetting() {
        super();
    }


    public UserSetting(UUID id ) {
        super( id );
    }

}
