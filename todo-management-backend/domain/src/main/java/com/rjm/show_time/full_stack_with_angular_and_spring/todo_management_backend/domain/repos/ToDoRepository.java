package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.repos;

import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;


@Transactional( readOnly = true )
public interface ToDoRepository extends JpaRepository< ToDo, UUID > {


    @Query( "from ToDo td where td.creator.email = :email order by td.creationDate desc")
    List< ToDo > findByCreator( @Param( value = "email" ) String email );

}
