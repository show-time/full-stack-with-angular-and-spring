package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.autoconfig;

import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.Constants;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@Configuration
@EntityScan( basePackages = Constants.ENTITIES_BASE_PACKAGE )
@EnableJpaRepositories( basePackages = Constants.REPOS_BASE_PACKAGE )
public class TodoManagementBackendDomainAutoConfiguration {
}
