package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.example_service.api;


public class Endpoints {

    public static final String ROOT = "/";


    public static final String API_BASE = "/api";


    public static final String HELLO = API_BASE + "/" + "hello";

}
