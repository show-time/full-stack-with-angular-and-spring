package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.api;


public class Endpoints {

    public static final String ROOT = "/";


    public static final String API_BASE = "/api";


    public static final String TODO_BASE = API_BASE + "/" + "todo";

    public static final String FIND_ALL_TODOS = TODO_BASE + "/" ;

    public static final String CREATE_TODO = TODO_BASE;

    public static final String FIND_TODO_BY_ID = TODO_BASE + "/" + "{id}" ;

    public static final String DELETE_TODO_BY_ID = TODO_BASE + "/" + "{id}";

    public static final String UPDATE_TODO_BY_ID = TODO_BASE + "/" + "{id}";

    public static final String FIND_TODOS_BY_CREATOR = TODO_BASE + "/" + "creator" + "/" + "{email}";

}
