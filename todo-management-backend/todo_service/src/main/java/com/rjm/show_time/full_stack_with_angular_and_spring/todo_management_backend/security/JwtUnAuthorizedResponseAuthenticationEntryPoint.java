package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.security;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class JwtUnAuthorizedResponseAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {


	private static final long serialVersionUID = -8970718410437077606L;


	@Override
	public void commence( HttpServletRequest request, HttpServletResponse response,
						  AuthenticationException authException ) throws IOException {

		log.error( "Captured security exception", authException );

		response.sendError( HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage() );
	}

}
