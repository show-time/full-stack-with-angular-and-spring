package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.api;

import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities.ToDo;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities.User;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.repos.ToDoRepository;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.repos.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Slf4j
@RestController
@CrossOrigin( value = "http://localhost:4200" )
public class ToDoRestController {


    private final ToDoRepository toDoRepository;

    private final UserRepository userRepository;


    public ToDoRestController(UserRepository userRepository, ToDoRepository toDoRepository) {
        this.userRepository = userRepository;
        this.toDoRepository = toDoRepository;
    }


    @GetMapping( value = Endpoints.FIND_ALL_TODOS )
    public List< ToDo > findAll() {

        return this.toDoRepository.findAll();
    }


    @GetMapping( value = Endpoints.FIND_TODO_BY_ID )
    public ResponseEntity< ToDo > findById( @PathVariable( name = "id" ) UUID id ) {

        Optional< ToDo > optionalToDo = this.toDoRepository.findById( id );

        if( optionalToDo.isPresent() ) {

            return ResponseEntity.ok(optionalToDo.get());
        }

        return ResponseEntity.notFound().build();
    }


    @GetMapping( value = Endpoints.FIND_TODOS_BY_CREATOR )
    public List< ToDo > findByCreator( @PathVariable(value = "email" ) String email ) {

        return this.toDoRepository.findByCreator( email );
    }


    @DeleteMapping( value = Endpoints.DELETE_TODO_BY_ID )
    public ResponseEntity< Void > delete( @PathVariable( value = "id" ) UUID id ) {

        Optional< ToDo > optionalToDo = this.toDoRepository.findById( id );

        if( optionalToDo.isPresent() ) {

            this.toDoRepository.deleteById( id );

            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.notFound().build();
    }


    @PutMapping( Endpoints.UPDATE_TODO_BY_ID )
    public ResponseEntity< ToDo > update( @RequestBody ToDo toDo ) {

        UUID id = toDo.getId();

        if( id == null ) {

            return ResponseEntity.badRequest().build();
        }

        Optional< ToDo > optionalToDo = this.toDoRepository.findById( id );

        if( !optionalToDo.isPresent() ) {

            return ResponseEntity.notFound().build();
        }

        ToDo updated = this.toDoRepository.save( toDo );

        return ResponseEntity.ok( updated );
    }


    @PostMapping( Endpoints.CREATE_TODO )
    public ResponseEntity< Void > create( @RequestBody ToDo toDo ) {

        Optional< User > optionalUser = this.userRepository.findById( toDo.getCreator().getId() );

        if( !optionalUser.isPresent() ) {

            return ResponseEntity.badRequest().build();
        }

        toDo.setCreator( optionalUser.get() );

        ToDo created = this.toDoRepository.save( toDo );

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path( "/{id}" )
                .buildAndExpand( created.getId() )
                .toUri();

        return ResponseEntity.created( uri ).build();
    }

}
