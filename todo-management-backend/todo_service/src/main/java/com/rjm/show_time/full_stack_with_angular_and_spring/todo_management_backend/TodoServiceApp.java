package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TodoServiceApp {

    public static void main( String[] args ) {

        SpringApplication.run( TodoServiceApp.class, args );
    }
}
