package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.user_service.autoconfig;

import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.user_service.security.JwtTokenAuthorizationOncePerRequestFilter;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.user_service.security.JwtUnAuthorizedResponseAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityAutoConfiguration extends WebSecurityConfigurerAdapter {


    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenAuthorizationOncePerRequestFilter jwtAuthenticationTokenFilter;

    @Autowired
	private JwtUnAuthorizedResponseAuthenticationEntryPoint jwtUnAuthorizedResponseAuthenticationEntryPoint;


	@Bean
	public PasswordEncoder passwordEncoderBean() {

		return new BCryptPasswordEncoder();
	}


    @Autowired
    public void configureGlobal( AuthenticationManagerBuilder auth ) throws Exception {

        auth.userDetailsService( this.userDetailsService ).passwordEncoder( passwordEncoderBean() );
    }


	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {

		return super.authenticationManagerBean();
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.headers().frameOptions().disable();

		http
			.csrf().disable() //csrf not needed for jwt authentication

			.exceptionHandling()
				.authenticationEntryPoint( this.jwtUnAuthorizedResponseAuthenticationEntryPoint )

			.and()
				.sessionManagement().sessionCreationPolicy( SessionCreationPolicy.STATELESS )

			.and()
				.authorizeRequests()
                .antMatchers( HttpMethod.GET,"/" ).permitAll() // allow to ping the root
                .antMatchers( HttpMethod.OPTIONS, "/**" ).permitAll() //allow options on each and every endpoint

            .and()
                .authorizeRequests()
                .antMatchers( "/**" ).authenticated() // all other endpoints require jwt authentication

			.and()
				.addFilterBefore( this.jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class );
	}

}
