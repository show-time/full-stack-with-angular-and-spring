package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.user_service.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@CrossOrigin( value = "http://localhost:4200" )
public class WelcomeRestController {


    @GetMapping( value = Endpoints.WELCOME )
    public WelcomeData getWelcomeData( @PathVariable( name = "email" ) String email ) {

        return new WelcomeData( "Hello from the backend, dear " + email );
    }


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class WelcomeData {

        private String message;
    }
}
