package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.user_service.security;

import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities.User;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class JwtUserDetailsService implements UserDetailsService {


    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername( String userName ) throws UsernameNotFoundException {

        Optional< User > optionalUser = this.userRepository.findByEmail( userName );

        if( !optionalUser.isPresent() ) {

            throw new UsernameNotFoundException(String.format( "No user found with email %s", userName ) );
        }

        User user = optionalUser.get();

        return new JwtUserDetails( user.getEmail(), user.getPassword() );
    }

}
