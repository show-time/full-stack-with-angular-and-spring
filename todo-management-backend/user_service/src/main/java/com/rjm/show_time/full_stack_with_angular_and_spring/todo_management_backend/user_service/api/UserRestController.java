package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.user_service.api;

import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.entities.User;
import com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.domain.repos.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;


@Slf4j
@RestController
@CrossOrigin( value = "http://localhost:4200" )
public class UserRestController {


    private final UserRepository userRepository;


    public UserRestController( UserRepository userRepository ) {
        this.userRepository = userRepository;
    }


    @GetMapping( value = Endpoints.FIND_USER_BY_EMAIL )
    public ResponseEntity< User > findByEmail( @PathVariable( name = "email" ) String email ) {

        Optional< User > optionalUser = this.userRepository.findByEmail( email );

        if( optionalUser.isPresent() ) {

            return ResponseEntity.ok( optionalUser.get() );
        }

        return ResponseEntity.notFound().build();
    }


    @GetMapping( value = Endpoints.FIND_USER_UUID_BY_EMAIL_AND_PASSWORD )
    public ResponseEntity< Long > findUUIdByEmailAndPassword( @PathVariable( name = "email") String email,
                                                              @PathVariable( name = "password") String password ) {

        Optional< Long > optionalUserId = this.userRepository.findIdByEmailAndPassword( email, password );

        if( optionalUserId.isPresent() ) {

            return ResponseEntity.ok( optionalUserId.get() );
        }

        return ResponseEntity.notFound().build();
    }


}
