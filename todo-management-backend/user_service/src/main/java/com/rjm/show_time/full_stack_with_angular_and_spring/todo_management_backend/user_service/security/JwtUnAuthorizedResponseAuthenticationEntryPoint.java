package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.user_service.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;


@Slf4j
@Component
public class JwtUnAuthorizedResponseAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {


	private static final long serialVersionUID = -8970718410437077606L;


	@Override
	public void commence( HttpServletRequest request, HttpServletResponse response,
						  AuthenticationException authException ) throws IOException {

		log.error( "Captured security exception", authException );

		response.sendError( HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage() );
	}

}
