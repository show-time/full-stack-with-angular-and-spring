package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.user_service.api;


public class Endpoints {


    private static final String API_BASE = "/api";


    private static final String WELCOME_BASE = API_BASE + "/" + "welcome";

    public static final String WELCOME = WELCOME_BASE + "/" + "{email}";


    private static final String USERS_BASE = API_BASE + "/" + "users";

    public static final String FIND_USER_BY_EMAIL = USERS_BASE + "/email/" + "{email}";

    public static final String FIND_USER_UUID_BY_EMAIL_AND_PASSWORD =
            USERS_BASE
            + "/" + "email" + "/" + "{email}"
            + "/" + "password" + "/" + "{password}"
            + "/" + "uuid";

}
