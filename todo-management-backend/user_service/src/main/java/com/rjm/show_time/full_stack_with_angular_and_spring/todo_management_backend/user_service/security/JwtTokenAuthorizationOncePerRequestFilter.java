package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.user_service.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Slf4j
@Component
public class JwtTokenAuthorizationOncePerRequestFilter extends OncePerRequestFilter {


	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
    private UserDetailsService userDetailsService;


	@Override
	protected void doFilterInternal( HttpServletRequest request,
									 HttpServletResponse response,
									 FilterChain chain ) throws ServletException, IOException {

		String authHeader = request.getHeader( "Authorization" );

		if( StringUtils.hasText( authHeader ) ) {

			if( authHeader.startsWith( "Bearer ") ) {

				String token = authHeader.substring("Bearer ".length());

				String username = this.jwtTokenUtil.getUsernameFromToken( token );

                UserDetails userDetails = this.userDetailsService.loadUserByUsername( username );

				if ( userDetails != null && SecurityContextHolder.getContext().getAuthentication() == null) {

					if (this.jwtTokenUtil.validateToken(token, userDetails)) {

						UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
								userDetails, null, userDetails.getAuthorities());

						usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

						SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
					}
				}
			}
		}

		chain.doFilter(request, response);
	}

}
