package com.rjm.show_time.full_stack_with_angular_and_spring.todo_management_backend.user_service.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@CrossOrigin( value = "http://localhost:4200" )
public class PingRestController {


    @GetMapping( value = "/" )
    public String ping() {

        return "pong";
    }

}
